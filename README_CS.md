# DjangoCMS Mapy.cz Markers

Projekt *DjangoCMS Mapy.cz Markers* je plugin pro [DjangoCMS](https://www.django-cms.org/) framework.
Plugin zobrazuje mapu Česka se značkami. Mapu vytváří server [Mapy.cz](https://mapy.cz).
Plugin byl vytvořen pro verzi [Mapy API version 4.13 – Neil Armstrong](https://api.mapy.cz/).

## Použití

### Plugin "Mapy.cz Markers"

Plugin **Mapy.cz Markers** zobrazuje mapu Česka. Lze nastavit velikost mapy, umístění a velikost přiblížení. Dále je možné vypnout ovladače mapy (posuvník a typy mapových podkladů) a přibližování kolečkem myši. Ve výchozím nastavení se značky, které jsou blízko sebe, automaticky shlukují do skupin. Tuto vlastnost lze také vypnout.

![Plugin Mapy.cz](https://gitlab.nic.cz/djangocms-apps/djangocms-mapycz-markers/-/raw/main/screenshots/plugin-mapycz.png)

![Plugin s clusterem](https://gitlab.nic.cz/djangocms-apps/djangocms-mapycz-markers/-/raw/main/screenshots/map-with-cluster.png)

### Plugin "Markers"

Plugin **Značka** zobrazuje značku na mapě. Každá značka obsazuje adresu bodu na mapě a jeho přesnou polohu, udávanou zeměpisnou šířkou a délkou. Při zadávání adresy se zobrazí našeptávač se seznamem známých adres. Při výběru ze seznamu se do značky automaticky doplní její zeměpisná šířka a délka.

Dále je možné nastavit název značky, který se u ikony zobrazuje jako atribut *title*. Lze nastavit barvu ikony nebo ji přenasavit na svou vlastní.

Do značky je možné zadat vizitku. Obsah vizitky se skládá ze záhlaví, těla a zápatí.

![Plugin Marker na Mapy.cz](https://gitlab.nic.cz/djangocms-apps/djangocms-mapycz-markers/-/raw/main/screenshots/choose-marker-address.png)

### Panorama

Pokud se do stránky vloží plugin *Styl* s třídou `mapycz-panorama`, tak se v zápatí vizitky zobrazí odkaz *Panorama*. Kliknutím na něj se v pluginu *Styl* zobrazí panorama daného bodu (je-li v mapě k dispozici).

![Mapa s panorama](https://gitlab.nic.cz/djangocms-apps/djangocms-mapycz-markers/-/raw/main/screenshots/map-with-panorama.png)

### Plugin "Propojit adresu s mapou"

Plugin **Propojit adresu s mapou** lze použít k dvěma účelům.

Prvním účelem je našeptávač. Do pole *Adresa* se zadá název textového pole, u kterého bude našeptávač zobrazovat seznam známých adres. Jakmile uživatel začne psát adresu, zobrazí se mu seznam adres pasujících k té psané. Kliknutím do seznamu je adresa vložena do pole adresy.

Druhým účelem je propojení s mapou. Při něm se nastaví vložení zeměpisných souřadnic vybrané adresy do příslušných polí formuláře. Dále lze definovat, která pole formuláře představují obsah vizity.

Pozor! Plugin musí být umístěný pod pluginem **Formulář**. Používá textová pole jen svého *nadřazeného* formulálře.

![Formulář s našeptávačem adresy](https://gitlab.nic.cz/djangocms-apps/djangocms-mapycz-markers/-/raw/main/screenshots/address-whisperer.png)

![Propojení adresy s mapou](https://gitlab.nic.cz/djangocms-apps/djangocms-mapycz-markers/-/raw/main/screenshots/connect-address-with-map.png)

### Příkaz "Přidat značky do Mapy.cz"

Při tomto propojení s mapou lze pak uložené odpovědi uživatelů vkládat do mapy jako značky. Editor webu v seznamu *Odeslané formuláře* vybere odpovědi uživatelů a v seznamu akcí nastaví akci *Přidat značky do Mapy.cz*. Kliknutím na tlačítko *Provést* se z údajů od uživatelů vytvoří na mapě značky.

![Příkaz](https://gitlab.nic.cz/djangocms-apps/djangocms-mapycz-markers/-/raw/main/screenshots/run-action-markers-into-map.png)

### Funkce form_submission_field

Funkce `form_submission_field` umožní v seznamu odeslaných formulářů zobrazit odkazy na stránky, na kterých se v mapě vyskytuje Marker uvedený v odeslaných datech.

## Instalace

Více informací (anglicky) v [INSTALL.md](https://gitlab.nic.cz/djangocms-apps/djangocms-mapycz-markers/-/blob/main/INSTALL.md).

## Ukázkový příklad

Jak spustit příklad (anglicky) naleznete na [example/HOWTO.md](https://gitlab.nic.cz/djangocms-apps/djangocms-mapycz-markers/-/blob/main/example/HOWTO.md).


## Autor

Zdeněk Böhm zdenek.bohm@nic.cz
CZ.NIC, z. s. p. o.

## Licence

[GPLv3+](https://www.gnu.org/licenses/gpl-3.0.html)
