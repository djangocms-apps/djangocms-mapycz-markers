from typing import Optional

from cms.api import add_plugin
from cms.models.placeholdermodel import Placeholder
from django.http import HttpRequest

from djangocms_mapycz_markers.cms_plugins import MarkerCMSPlugin
from djangocms_mapycz_markers.models import MapPlugin, MarkerPlugin
from djangocms_mapycz_markers.utils import keep_only_tags


def add_plugin_marker_into_map(
    request: HttpRequest,
    placeholder: Placeholder,
    language: str,
    target: MapPlugin,
    latitude: str,
    longitude: str,
    address: str,
    title: Optional[str],
    card_header: Optional[str],
    card_body: Optional[str],
    card_footer: Optional[str]
) -> Optional[MarkerPlugin]:
    """Add plugin Marker into the map."""
    return add_plugin(
        placeholder,
        MarkerCMSPlugin,
        language,
        target=target,
        latitude=latitude,
        longitude=longitude,
        address=address,
        title=title,
        card_header=keep_only_tags(card_header),
        card_body=keep_only_tags(card_body),
        card_footer=keep_only_tags(card_footer)
    )
