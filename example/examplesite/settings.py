"""Example settings."""
import os

DEBUG = True
SECRET_KEY = "secret"
SITE_ID = 1
STATIC_URL = '/static/'
ROOT_URLCONF = 'examplesite.urls'

MEDIA_URL = 'media/'
MEDIA_ROOT = 'media/'

BASE_DIR = os.path.join(os.path.dirname(os.path.abspath(__file__)))
STATICFILES_DIRS = [os.path.join(BASE_DIR, 'static')]

INSTALLED_APPS = (
    # Django apps requirted set.
    "django",
    "django.contrib.admin",
    "django.contrib.auth",
    "django.contrib.contenttypes",
    "django.contrib.messages",
    "django.contrib.sessions",
    "django.contrib.sitemaps",
    "django.contrib.sites",
    "django.contrib.staticfiles",
    "django.forms.fields",
    'sekizai',
    "cms",
    "menus",
    "treebeard",
    "djangocms_style",
    "djangocms_text_ckeditor",
    # Dependencies for Aldryn forms.
    "easy_thumbnails",
    "filer",
    "aldryn_forms",
    # The plugin.
    'djangocms_mapycz_markers',
)

DATABASES = {
    "default": {
        "ENGINE": "django.db.backends.sqlite3",
        "NAME": "db.sqlite3",
    }
}

LANGUAGES = (
    ('en', 'English'),
)
LANGUAGE_CODE = 'en'

TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': ['examplesite/templates'],
        'OPTIONS': {
            'context_processors': [
                # Django's defaults.
                'django.template.context_processors.debug',
                'django.template.context_processors.request',
                'django.contrib.auth.context_processors.auth',
                'django.contrib.messages.context_processors.messages',
                'django.template.context_processors.i18n',  # Needed by Django CMS.

                # Django CMS's core context processors.
                'cms.context_processors.cms_settings',
                'sekizai.context_processors.sekizai',  # Static file management for template blocks.
            ],
            'loaders': [
                'django.template.loaders.filesystem.Loader',
                'django.template.loaders.app_directories.Loader',
            ],
        },
    },
]

MIDDLEWARE = [
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.locale.LocaleMiddleware',
    'cms.middleware.user.CurrentUserMiddleware',
    'cms.middleware.page.CurrentPageMiddleware',
    'cms.middleware.toolbar.ToolbarMiddleware',
    'cms.middleware.language.LanguageCookieMiddleware',
]


CMS_TEMPLATES = [
    # optional templates that extend base.html, to be used with Bootstrap 5
    ('bootstrap5.html', 'Bootstrap 5 Demo'),

    # a minimal template to get started with
    ('minimal.html', 'Minimal template'),
]

# Custom Action of plugin "Mapy.cz Markers":
MAPYCZ_ADD_MARKER_TO_MAP = "examplesite.utils.plugin_mapycz.add_plugin_marker_into_map"

ALDRYN_FORMS_SUBMISSION_LIST_DISPLAY_FIELD = "djangocms_mapycz_markers.utils_admin.form_submission_field"
