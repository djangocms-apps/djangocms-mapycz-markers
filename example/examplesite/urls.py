from aldryn_forms.cms_plugins import FormPlugin
from cms.plugin_pool import plugin_pool
from django.conf import settings
from django.conf.urls.static import static
from django.contrib import admin
from django.contrib.admin import site
from django.urls import include, path

from djangocms_mapycz_markers.actions import mapycz_add_marker
from djangocms_mapycz_markers.urls import urlpatterns as plugin_urlpatterns

# Aldryn forms version <=6.2.1 not register the plugin Form.
plugin_pool.register_plugin(FormPlugin)

site.add_action(mapycz_add_marker)

urlpatterns = [
    path('admin/', admin.site.urls),
] + plugin_urlpatterns

urlpatterns.append(path('', include('cms.urls')))

if settings.DEBUG:
    urlpatterns.extend(static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT),)

# the new django admin sidebar is bad UX in django CMS custom admin views.
admin.site.enable_nav_sidebar = False
