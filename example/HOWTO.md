How to run the example
======================

Fetch the project:

```
git clone https://gitlab.nic.cz/djangocms-apps/djangocms-mapycz-markers.git
```

Install site:

```
cd djangocms-mapycz-markers/example/
virtualenv env
source env/bin/activate
pip install -r requirements.txt
```

Run webserever:

```
python manage.py runserver
```

Open the site in your browser at http://127.0.0.1:8000/ or http://localhost:8000/.
Login to the site administration at http://localhost:8000/admin with username `admin` and password `admin`.
The Map is on the main page at http://127.0.0.1:8000/. The form is http://127.0.0.1:8000/form/.
There are also posts with markers ready to insert to the map. See at Admininistaton / Form submission / Action: Add Markers into Mapy.cz - select posts and press button "Go".

Deactivate virtual environment after stop runserver:

```
deactivate
```
