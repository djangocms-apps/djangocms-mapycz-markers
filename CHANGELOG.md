# Changelog
All notable changes to this project are documented in this file. The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/), and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]

## [1.1.0] - 2023-01-24

- Add form_submission_field into utils_admin.

## [1.0.0] - 2023-01-11
### Added
- Plugins MapCMSPlugin, MarkerCMSPlugin, ConnectorCMSPlugin.


[Unreleased]: https://gitlab.nic.cz/djangocms-apps/djangocms-mapycz-markers/-/compare/1.1.0...main
[1.1.0]: https://gitlab.nic.cz/djangocms-apps/djangocms-mapycz-markers/-/compare/1.0.0...1.1.0
[1.0.0]: https://gitlab.nic.cz/djangocms-apps/djangocms-mapycz-markers/-/compare/4ed0b618f1300d...1.0.0
