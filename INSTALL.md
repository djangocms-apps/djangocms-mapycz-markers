# Installation

Install from pypi.org:

```
pip install djangocms-mapycz-markers
```

`site/settings.py`:

```python
INSTALLED_APPS = [
    ...
    'djangocms_mapycz_markers',
    ...
]
```

Add map urls into main urls.py:

```python
from djangocms_mapycz_markers.urls import urlpatterns as plugin_urlpatterns

urlpatterns += plugin_urlpatterns
```

Add block render_block "base_js" into main template:

```html
{% render_block "base_js" %}
```

### Use with Aldryn forms

Install Aldryn forms:

```
pip install aldryn-forms
```

`site/settings.py`:

```python
INSTALLED_APPS = [
    ...
    'aldryn_forms',
    'djangocms_mapycz_markers',
    ...
]
```


The actions for writing tags are set in the `admin.py` module of the site project:

```python
from django.contrib.admin import site
from djangocms_mapycz_markers.actions import mapycz_add_marker


site.add_action(mapycz_add_marker)
```

The content of the visit can be written in HTML code. For this reason, it is advisable to use some function that handles *unsafe* tags. To this end, the project allows you to define a custom function to store the tags. There are also `keep_only_tags` and `default_field_formatter` functions in the `utils` project that will keep only selected HTML tags and attributes from the content.

`site/settings.py`:

```python
MAPYCZ_ADD_MARKER_TO_MAP = "site.utils.add_plugin_marker_into_map"
MAPYCZ_FIELD_FORMATTER = "site.utils.default_field_formatter"
```

## Example

How to run the example. For more see [example/HOWTO.md](https://gitlab.nic.cz/djangocms-apps/djangocms-mapycz-markers/-/blob/main/example/HOWTO.md).
